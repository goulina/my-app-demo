import 'package:my_app_demo/generated/json/base/json_convert_content.dart';
import 'package:my_app_demo/models/user_entity.dart';
import 'package:flutter/material.dart';

import 'package:flutter/foundation.dart';


UserEntity $UserEntityFromJson(Map<String, dynamic> json) {
  final UserEntity userEntity = UserEntity();
  final String? account = jsonConvert.convert<String>(json['account']);
  if (account != null) {
    userEntity.account = account;
  }
  final String? password = jsonConvert.convert<String>(json['password']);
  if (password != null) {
    userEntity.password = password;
  }
  return userEntity;
}

Map<String, dynamic> $UserEntityToJson(UserEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['account'] = entity.account;
  data['password'] = entity.password;
  return data;
}

extension UserEntityExtension on UserEntity {
  UserEntity copyWith({
    String? account,
    String? password,
  }) {
    return UserEntity()
      ..account = account ?? this.account
      ..password = password ?? this.password;
  }
}