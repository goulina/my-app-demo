import 'package:my_app_demo/generated/json/base/json_convert_content.dart';
import 'package:my_app_demo/models/todo_list_entity.dart';

TodoListEntity $TodoListEntityFromJson(Map<String, dynamic> json) {
  final TodoListEntity todoListEntity = TodoListEntity();
  final String? title = jsonConvert.convert<String>(json['title']);
  if (title != null) {
    todoListEntity.title = title;
  }
  final String? type = jsonConvert.convert<String>(json['type']);
  if (type != null) {
    todoListEntity.type = type;
  }
  final String? time = jsonConvert.convert<String>(json['time']);
  if (time != null) {
    todoListEntity.time = time;
  }
  final bool? isDone = jsonConvert.convert<bool>(json['isDone']);
  if (isDone != null) {
    todoListEntity.isDone = isDone;
  }
  return todoListEntity;
}

Map<String, dynamic> $TodoListEntityToJson(TodoListEntity entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['title'] = entity.title;
  data['type'] = entity.type;
  data['time'] = entity.time;
  data['isDone'] = entity.isDone;
  return data;
}

extension TodoListEntityExtension on TodoListEntity {
  TodoListEntity copyWith({
    String? title,
    String? type,
    String? time,
    bool? isDone,
  }) {
    return TodoListEntity()
      ..title = title ?? this.title
      ..type = type ?? this.type
      ..time = time ?? this.time
      ..isDone = isDone ?? this.isDone;
  }
}