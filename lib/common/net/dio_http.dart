import 'package:dio/dio.dart';

class DioHttp {
  late Dio _dio;

  static DioHttp getInstance(){
    _instance ??= DioHttp._internal();
    return _instance!;
  }

  static DioHttp? _instance=DioHttp._internal();

  DioHttp._internal(){
    BaseOptions baseOptions=BaseOptions(
      baseUrl:"http://10.222.151.143:8888/",
      // connectTimeout: const Duration(microseconds: 1000*100),
      // receiveTimeout: const Duration(microseconds: 1000*100),
    );

    _dio = Dio(baseOptions) ;
  }

  Future request<T>(String method, String url, {
    Object? data,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
  }) async {
    final Response response = await _dio.request(
      url,
      data: data,
      queryParameters: queryParameters,
      options: _checkOptions(method, options),
      cancelToken: cancelToken,
    );
    try {
      return response.data;
    } catch(e) {
      print(e);
      return e.toString();
    }
  }


  Options _checkOptions(String method, Options? options) {
    options ??= Options();
    options.method = method;
    return options;
  }
}