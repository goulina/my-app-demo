import 'package:flutter/material.dart';
import 'package:my_app_demo/models/todo_list_entity.dart';

class TodoListItem extends StatelessWidget{
  TodoListEntity todo;

  TodoListItem(this.todo, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:BoxDecoration(border:Border(bottom: BorderSide(width: 1.0, color: Colors.grey.shade300),)),
      padding: const EdgeInsets.all(15.0),
      child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Padding(
              padding:  const EdgeInsets.only(right: 20.0),
              child:  Icon(Icons.check_circle,color: todo.isDone! ? Colors.red:Colors.grey,),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(todo.title!,textScaleFactor:1.2),
                Text(todo.type!)
              ],
            ),
          ],
        ),
        Text(todo.time!)
      ],
    ),
    );
  }

}

