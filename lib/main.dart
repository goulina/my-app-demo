import 'package:flutter/material.dart';
import 'package:my_app_demo/routes/login/login_page.dart';
import 'package:my_app_demo/routes/todoList/todoList_detail_page.dart';
import 'package:my_app_demo/routes/todoList/todoList_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Map<String, WidgetBuilder>? routes;

  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
      ),
      routes: {
        "login_page": (context) => const LoginPage(),
        "todo_list_page": (context) => TodoListPage(),
        "todo_detail_page": (context) => TodoDetailPage(),
      },
      home: const LoginPage(),
    );
  }
}