import 'package:flutter/foundation.dart';
import 'package:my_app_demo/generated/json/base/json_field.dart';
import 'package:my_app_demo/generated/json/todo_list_entity.g.dart';
import 'dart:convert';
export 'package:my_app_demo/generated/json/todo_list_entity.g.dart';

@JsonSerializable()
class TodoListEntity extends ChangeNotifier{
	String? title;
	String? type;
	String? time;
	bool? isDone;

	TodoListEntity();


	TodoListEntity.init(this.title, this.type, this.time, this.isDone);

  factory TodoListEntity.fromJson(Map<String, dynamic> json) => $TodoListEntityFromJson(json);

	Map<String, dynamic> toJson() => $TodoListEntityToJson(this);

	void changeDoneState(){
		isDone = !isDone!;
	}

	@override
	String toString() {
		return jsonEncode(this);
	}
}