import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:my_app_demo/generated/json/base/json_field.dart';
import 'package:my_app_demo/generated/json/user_entity.g.dart';
import 'dart:convert';
export 'package:my_app_demo/generated/json/user_entity.g.dart';

@JsonSerializable()
class UserEntity extends ChangeNotifier{
	String? account="";
	String? password="";

	UserEntity();

	factory UserEntity.fromJson(Map<String, dynamic> json) => $UserEntityFromJson(json);

	Map<String, dynamic> toJson() => $UserEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}

	void changeAccount(String account){
		this.account = account;
		notifyListeners();
	}
}