import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:package_info_plus/package_info_plus.dart';
import '../../common/net/dio_http.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  LoginPageState createState() => LoginPageState();

}

class LoginPageState extends State<LoginPage> {
  final TextEditingController _unameController = TextEditingController();
  final TextEditingController _pwdController = TextEditingController();
  final GlobalKey _formKey = GlobalKey<FormState>();
  final String _loginText = '登录';
  final String _loggingInText = '正在登录...';
  bool _isLogging = false;
  bool _pwdHide = true;
  bool _nameAutoFocus = true;
  bool _nameClear = false;
  bool _pwdClear = false;
  bool _isButton1Disabled = true;

  @override
  void initState() {
    if(_unameController.text.isNotEmpty) {
      _nameAutoFocus = false;
    }
    _unameController.addListener((){
      if(_unameController.text.trim()!=""){
        _nameClear = true;
        setState(() {
        });
      }
    });
    _pwdController.addListener(() {
      if(_pwdController.text.trim()!=""){
        _pwdClear = true;
        setState(() {
        });
      }
    });
    super.initState();
  }

  Future<String> getPackageInfo() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }
  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: const EdgeInsets.fromLTRB(20,150,20,30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SvgPicture.asset('images/oocl_logo.svg'),
                  const SizedBox(height: 60),
                  getUsernameInput(),
                  const SizedBox(height: 25),
                  getPasswordInput(),
                  const SizedBox(height: 60),
                  getLoginButton()
                ],
              ),
              onChanged: (){
                if(_unameController.text.trim().isNotEmpty&&_pwdController.text.trim().isNotEmpty){
                  _isButton1Disabled = false;
                }else{
                  _isButton1Disabled = true;
                }
              },
            ),
            FutureBuilder(
                future: getPackageInfo(),
                builder: (BuildContext context, AsyncSnapshot snapshot){
                  if(!snapshot.hasData){
                    return const CircularProgressIndicator();
                  }
                  return Text(
                    'v${snapshot.data}',
                    style: const TextStyle(
                      color: Colors.grey
                    ),);
                })
          ],
        )

      ),
    );

  }

  Widget getUsernameInput() {
    return TextFormField(
      controller: _unameController,
      autofocus: _nameAutoFocus,
      decoration: InputDecoration(
        hintText: "账号",
        prefixIcon: const Icon(Icons.person),
        suffixIcon: _nameClear ? IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            setState(() {
              _unameController.text = "";
              _nameClear = false;
            });
          },
        ): null,
      ),
      validator: (v) {
        return v!.trim().isNotEmpty ? null : "账号不能为空";
      },
    );
  }

  Widget getPasswordInput() {
    return TextFormField(
      controller: _pwdController,
      autofocus: !_nameAutoFocus,
      decoration: InputDecoration(
        hintText: "密码",
        prefixIcon: const Icon(Icons.lock),
        suffixIcon: _pwdClear ? Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
                icon: Icon(_pwdHide ? Icons.visibility_off:Icons.visibility ),
                onPressed: () {
                  setState(() {
                    _pwdHide = !_pwdHide;
                  });
                }
            ),
            IconButton(
              icon: const Icon(Icons.clear),
              onPressed: () {
                setState(() {
                  _pwdController.text = "";
                  _pwdClear = false;
                });
              },
            ),
          ],
        ): null,
      ),
      obscureText:_pwdHide,
      validator: (v) {
        return v!.trim().isNotEmpty ? null : "密码不能为空";
      },
    );
  }

  Widget getLoginButton() {
    return Builder(
        builder: (BuildContext context){
          return Container(
            height: 40,
            width: double.infinity,
            decoration: BoxDecoration(
              color: _isButton1Disabled ? Colors.grey[300]: Theme.of(context).colorScheme.primary,
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: TextButton(
              style: ButtonStyle(
                foregroundColor: _isButton1Disabled ? MaterialStateProperty.all<Color>(Colors.grey):MaterialStateProperty.all<Color>(Colors.white),
              ),
              onPressed: _isButton1Disabled ? null:() async {
                if ((_formKey.currentState as FormState).validate()) {
                  var response = await login(_unameController.text.trim(), _pwdController.text.trim());
                  if(!response["success"]){
                    if(mounted){
                      showLoginFailedToast(context);
                    }
                    return;
                  }
                  modifyButtonStatus(true);
                  saveData(response["data"]["token"]);
                  if(mounted){
                    Navigator.of(context).pushNamed("todo_list_page");
                  }
                }
              },
              child: Text(_isLogging ? _loggingInText:_loginText),
            ),
          );
        });
  }

  Future<Map<String,dynamic>> login (String uname,String password) async{
    return await DioHttp.getInstance().request("post","user/login",
        data: {
          "account": uname,
          "password": password
        },
        options: Options(
            headers: {"Content-Type":"application/json"}
        ));
  }

  void showLoginFailedToast(BuildContext context) {
      const snackBar = SnackBar(
          content: Center(child: Text("LOGIN_FAILED-账号或密码不正确")),
          duration: Duration(milliseconds: 3000),
          behavior:SnackBarBehavior.floating);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void saveData(String token) async{
    SharedPreferences? sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("account", _unameController.text);
    sharedPreferences.setString("token", token);
  }

  void modifyButtonStatus(bool state){
    setState(() {
      _isLogging = state;
      _isButton1Disabled = state;
    });
  }
}