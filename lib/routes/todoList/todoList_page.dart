import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:my_app_demo/generated/json/base/json_convert_content.dart';
import 'package:my_app_demo/models/todo_list_entity.dart';
import 'package:my_app_demo/models/user_entity.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../common/net/dio_http.dart';
import '../../widgets/todoList/todoList_item.dart';

class TodoListPage extends StatefulWidget{
  TodoListPage({super.key});
  @override
  TodoListEntityPageState createState() => TodoListEntityPageState();
}

class TodoListEntityPageState extends State<TodoListPage>{

  List<TodoListEntity> _todoListList = <TodoListEntity>[];
  UserEntity? _currentUser ;



  @override
  void initState() {
    super.initState();
    _getTodoList();
    _getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _currentUser,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Theme.of(context).colorScheme.primary,
          centerTitle: true,
          title: Consumer<UserEntity?>(
            builder: (context, user, child){
              return Text(user?.account ?? '', style: const TextStyle(color: Colors.white));
            },
          ),
          actions: <Widget>[
            IconButton(
                icon: const Icon(Icons.exit_to_app),
                onPressed: () async{
                  String token = await _getToken();
                  var response = await DioHttp.getInstance().request("post", "user/logout",options: Options(
                    headers: {"Authorization": "Bearer $token"},
                  ));
                  if(response['success']){
                    clearSavedData();
                    if (mounted) {
                      var result = await Navigator.of(context).pushNamed("login_page");
                    }
                  }else{
                    if (mounted) {
                      showLogoutFailedToast(context);
                    }
                  }
                }),
          ],
        ),
        body: Center(
            child: ListView.builder(
                itemCount: _todoListList.length,
                itemBuilder: (BuildContext context, int index){
                  return GestureDetector(
                    onTap:() async{
                      var result = await Navigator.of(context).pushNamed("todo_detail_page", arguments: _todoListList[index]);
                      if(result!=null){
                        var resultIndex = _todoListList.indexWhere((element) => element.title == result);
                        setState(() {
                          _todoListList[resultIndex].isDone = true;
                        });
                      }

                    },
                    child: TodoListItem(_todoListList[index]),
                  );
                })
        ),
      ),
    );


  }

    Future<String> _getToken() async {
      SharedPreferences? sharedPreferences = await SharedPreferences.getInstance();
      return sharedPreferences.getString("token")!;
    }

    Future<void> _getTodoList() async {
      String token = await _getToken();
      final response = await DioHttp.getInstance().request(
        'get',
        "todoList",
        options: Options(
          headers: {"Authorization": "Bearer $token"},
        ),
      );
      print(response);
      if(response['success']) {
        _todoListList = JsonConvert().convertListNotNull<TodoListEntity>(
            response['data']) ?? [];
        setState(() {
        });
      }

    }

    void clearSavedData() async{
      SharedPreferences? sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.remove("account");
      sharedPreferences.remove("token");
    }

    void showLogoutFailedToast(BuildContext context) {
      const snackBar = SnackBar(
          content: Center(child: Text("LOGOUT_FAILED-退出登录失败")),
          duration: Duration(milliseconds: 3000),
          behavior:SnackBarBehavior.floating);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }

  Future<void> _getCurrentUser() async{
    SharedPreferences? sharedPreferences = await SharedPreferences.getInstance();
    _currentUser = UserEntity.fromJson(<String, dynamic>{"account":sharedPreferences.getString("account")});
    setState(() {
    });
  }
}