import 'package:flutter/material.dart';
import 'package:my_app_demo/models/todo_list_entity.dart';

class TodoDetailPage extends StatelessWidget {
  late TodoListEntity todo ;
  TodoDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    todo = ModalRoute
        .of(context)
        ?.settings
        .arguments as TodoListEntity;
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Theme.of(context).colorScheme.primary,
            centerTitle: true,
            title: Text(todo.type!, style: const TextStyle(color: Colors.white))
        ),
        body: Container(
          padding: const EdgeInsets.all(15.0),
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(todo.title!),
                  Text("时间：${todo.time}"),
                  Text("程度：${todo.type}")
                ],
              ),
              Center(
                child: ElevatedButton(
                  child: const Text("已办理"),
                  onPressed: () {
                    Navigator.pop(context, todo.title);
                  },
                ),
              )
            ],
          ),
        )
    );
  }
}